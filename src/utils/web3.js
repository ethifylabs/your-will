import { default as Web3 } from "web3";
import { ethers } from "ethers";
import sablierABI from "../Contract/sablierABI";
import daiABI from "../Contract/dai";
import cdaiABI from "../Contract/cdai";

class Web3Service {
  // kovan
  sablierAddress = "0xc04ad234e01327b24a831e3718dbfcbe245904cc";
  daiAddress = "0xc4375b7de8af5a38a93548eb8453a498222c4ff2";
  cdaiAddress = "0xe7bc397dbd069fc7d0109c0636d06888bb50668c";

  signer = new ethers.providers.Web3Provider(window.ethereum).getSigner();

  sablier = new ethers.Contract(this.sablierAddress, sablierABI, this.signer);
  dai = new ethers.Contract(this.daiAddress, daiABI, this.signer);

  cdai = new ethers.Contract(this.cdaiAddress, cdaiABI, this.signer);

  createStream = async (recipient, deposit, startTime, stopTime) => {
    const approveTx = await this.dai.approve(this.sablier.address, deposit); // approve the transfer
    await approveTx.wait();

    const createStreamTx = await this.sablier.createStream(
      recipient,
      deposit,
      this.dai.address,
      startTime,
      stopTime
    );
    await createStreamTx.wait();
  };

  createCompoundStream = async (
    recipient,
    deposit,
    senderSharePercentage,
    recipientSharePercentage,
    startTime,
    stopTime
  ) => {
    const approveTx = await this.cdai.approve(this.sablier.address, deposit); // approve the transfer
    await approveTx.wait();

    const createCompoundingStreamTx = await this.sablier.createCompoundingStream(
      recipient,
      deposit,
      this.cdai.address,
      startTime,
      stopTime,
      senderSharePercentage,
      recipientSharePercentage
    );
    await createCompoundingStreamTx.wait();
  };

  withdraw = async (streamId, amount) => {
    const withdrawFromStreamTx = await this.sablier.withdrawFromStream(
      streamId,
      amount
    );
    await withdrawFromStreamTx.wait();
  };

  cancel = async streamId => {
    const cancelStreamTx = await this.sablier.cancelStream(streamId);
    await cancelStreamTx.wait();
  };

  getAccount = async () => {
    let account = null;
    try {
      if (
        typeof window.ethereum !== "undefined" ||
        typeof window.web3 !== "undefined"
      ) {
        account = await window.ethereum.enable();
        window.web3 = new Web3(window.ethereum);
        window.readOnly = false;
      } else {
        window.web3 = new Web3(
          new Web3.providers.WebsocketProvider("wss://mainnet.infura.io/ws")
        );
        window.readOnly = true;
      }
    } catch (error) {
      console.log(error);
    }
    return account;
  };
}

export default new Web3Service();
