export function shortenEthAddr(str) {
  const shortenStr =
    str &&
    `${str.substring(0, 5)}...${str.substring(str.length - 5, str.length)}`;
  return shortenStr;
}
