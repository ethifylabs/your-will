import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import SendIcon from "@material-ui/icons/Send";
import {
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Divider,
  TextField,
  Button,
  Icon
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Web3Service from "../../utils/web3";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    position: "absolute",
    top: 0,
    right: 0,
    width: 550,
    height: "100%"
  },
  formControl: {
    margin: theme.spacing(1),
    width: "100%"
  },
  topFormControl: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    width: "100%"
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4)
  }
}));

const CreateStreamModal = props => {
  const classes = useStyles();
  const tokenList = [
    {
      address: "0x4f96fe3b7a6cf9725f59d353f723c1bdb64ca6aa",
      name: "Dai Stablecoin"
    }
  ];
  const [token, setToken] = React.useState(tokenList[0].name);
  const [amount, setAmount] = React.useState();
  const [recipient, setRecipient] = React.useState();
  const [yourShare, setYourShare] = React.useState();
  const [recipientShare, setRecipientShare] = React.useState();
  const [startDate, setStartDate] = React.useState(new Date());
  const [stopDate, setStopDate] = React.useState(new Date());

  const handleTokenChange = event => {
    setToken(event.target.value);
  };
  const handleAmountChange = event => {
    setAmount(event.target.value);
  };
  const handleRecipientChange = event => {
    setRecipient(event.target.value);
  };
  const handleYourShareChange = event => {
    setYourShare(event.target.value);
  };
  const handleRecipientShareChange = event => {
    setRecipientShare(event.target.value);
  };
  const handleStartDateChange = date => {
    console.log(Math.round(new Date(date).getTime() / 1000));
    setStartDate(date);
  };
  const handleStopDateChange = date => {
    setStopDate(date);
  };

  const createStream = async () => {
    if (props.isCompoundStream) {
      await Web3Service.createCompoundStream(
        recipient,
        Number.parseFloat(amount),
        yourShare,
        recipientShare,
        Math.round(new Date(startDate).getTime() / 1000),
        Math.round(new Date(stopDate).getTime() / 1000)
      );
    } else {
      await Web3Service.createStream(
        recipient,
        Number.parseFloat(amount),
        Math.round(new Date(startDate).getTime() / 1000),
        Math.round(new Date(stopDate).getTime() / 1000)
      );
    }
  };

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={props.open}
      onClose={props.handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Fade in={props.open}>
        <div className={classes.paper}>
          <Typography variant="h4" color="inherit" noWrap>
            {props.isCompoundStream
              ? "Create Compound Stream"
              : "Create Stream"}
          </Typography>
          <Divider className={classes.divider} />
          <FormControl className={classes.formControl}>
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
              What token do you want to use?
            </InputLabel>
            <Select
              labelId="demo-simple-select-placeholder-label-label"
              id="demo-simple-select-placeholder-label"
              value={token}
              onChange={handleTokenChange}
              displayEmpty
            >
              {tokenList.map(token => {
                return (
                  <MenuItem key={token.address} value={token.name}>
                    {token.name}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <TextField
            id="standard-required"
            label="How much do you want to stream?"
            defaultValue=""
            placeholder="0 Dai"
            className={classes.topFormControl}
            value={amount}
            onChange={handleAmountChange}
          />
          <TextField
            id="standard-required"
            label="Who is the recipient?"
            defaultValue=""
            placeholder="0x5d2629a9E885C5F0D558d6fE28A1f856ABdBDD54"
            className={classes.topFormControl}
            value={recipient}
            onChange={handleRecipientChange}
          />
          {props.isCompoundStream ? (
            <TextField
              id="standard-required"
              label="What's your share?"
              defaultValue=""
              placeholder="70"
              className={classes.topFormControl}
              value={yourShare}
              onChange={handleYourShareChange}
            />
          ) : null}
          {props.isCompoundStream ? (
            <TextField
              id="standard-required"
              label="What's recipient share?"
              defaultValue=""
              placeholder="30"
              className={classes.topFormControl}
              value={recipientShare}
              onChange={handleRecipientShareChange}
            />
          ) : null}

          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="When to start?"
              className={classes.topFormControl}
              value={startDate}
              onChange={handleStartDateChange}
            />
          </MuiPickersUtilsProvider>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="When to stop?"
              className={classes.topFormControl}
              value={stopDate}
              onChange={handleStopDateChange}
            />
          </MuiPickersUtilsProvider>
          <Button
            variant="contained"
            color="primary"
            className={classes.topFormControl}
            endIcon={<SendIcon />}
            onClick={createStream}
          >
            Create Stream
          </Button>
        </div>
      </Fade>
    </Modal>
  );
};

export default CreateStreamModal;
