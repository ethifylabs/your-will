import ApolloClient from "apollo-boost";
import React from "react";
import { render } from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";


const client = new ApolloClient({
  uri: "https://api.thegraph.com/subgraphs/name/sablierhq/sablier-kovan"
});

// let streamId = this.props

const STREAMS = gql`
  {
    streams(id: ) {
      id
      cancellation {
        recipientBalance
        recipientInterest
        timestamp
        txhash
      }
      deposit
      exchangeRateInitial
      ratePerSecond
      recipient
      recipientSharePercentage
      sender
      senderSharePercentage
      startTime
      stopTime
      timestamp
      token {
        id
        decimals
        name
        symbol
      }
      txs {
        id
        block
        event
        from
        timestamp
        to
      }
      withdrawals {
        id
        amount
      }
    }
  }
`;

function Streams() {
  const { loading, error, data } = useQuery(STREAMS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return data.stream.map(({}) => (
    <div>
      <p>
      </p>
    </div>
  ));
}


const App = () => (
  <ApolloProvider client={client}>
    <div>
      <h2> My first Apollo app🚀 </h2>{" "}
    </div>{" "}
  </ApolloProvider>
);

render(<App />, document.getElementById("root"));
