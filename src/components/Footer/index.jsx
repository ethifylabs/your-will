import React from "react";
import { Typography, Link, CssBaseline } from "@material-ui/core";
const Copyright = () => {
  return (
    <React.Fragment>
      <CssBaseline />
      <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © "}
        <Link color="inherit" href="https://material-ui.com/">
          Trellis Lab
        </Link>{" "}
        {new Date().getFullYear()}
        {"."}
      </Typography>
    </React.Fragment>
  );
};

export default Copyright;
