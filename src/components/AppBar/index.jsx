import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { CssBaseline, Container } from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/Dashboard";
import Web3Service from "../../utils/web3";
import { shortenEthAddr } from "../../utils/utils";
import makeBlockie from "ethereum-blockies-base64";

const useStyles = makeStyles(theme => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  toolbar: {
    flexWrap: "wrap"
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: theme.spacing(1, 1.5),
    textDecoration: "none"
  },
  margin: {
    margin: theme.spacing(1)
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  }
}));

const AppBarComponent = props => {
  const classes = useStyles();
  const [account, setAccount] = React.useState(null);

  const enableWeb3 = async () => {
    const account = await Web3Service.getAccount();
    console.log(account[0]);
    setAccount(account[0]);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar
        position="static"
        color="default"
        elevation={0}
        className={classes.appBar}
      >
        <Container fixed>
          <Toolbar className={classes.toolbar}>
            <Typography
              variant="h4"
              color="inherit"
              noWrap
              className={classes.toolbarTitle}
            >
              Your Will
            </Typography>
            <nav>
              <Button
                size="medium"
                color="primary"
                aria-label="add"
                variant="outlined"
                className={classes.margin}
                startIcon={<DashboardIcon className={classes.extendedIcon} />}
              >
                Dashboard
              </Button>
            </nav>
            <Button
              href="#"
              color={"primary"}
              variant="outlined"
              className={classes.link}
              onClick={enableWeb3}
              startIcon={
                account !== null ? (
                  <img
                    src={makeBlockie(account)}
                    alt="controller"
                    style={{ height: 24, width: 24, borderRadius: 100 }}
                  />
                ) : null
              }
            >
              {account === null ? "Connect" : shortenEthAddr(account)}
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
    </React.Fragment>
  );
};

export default AppBarComponent;
