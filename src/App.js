import React, { useEffect } from "react";
import "./App.css";
import AppBarComponent from "./components/AppBar";
import Main from "./components/Main";

function App() {
  return (
    <React.Fragment>
      <AppBarComponent />
      <Main />
    </React.Fragment>
  );
}

export default App;
